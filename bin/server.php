<?php

use React\EventLoop\LoopInterface;
use React\Http\Server as HttpServer;
use React\Socket\Server as TcpSocket;

require_once __DIR__.'/../vendor/autoload.php';

/**
 * We build the container
 * @var $container \Psr\Container\ContainerInterface
 */
$container = include __DIR__.'/../config/container.php';
/**
 * We fetch the Application out of it.
 * @var $app \Hermes\HttpApp\AppInterface
 */
$app = $container->get(\Hermes\HttpApp\AppInterface::class);
include __DIR__.'/../config/app-config.php';
/**
 * We open a TCP Socket, attach an Http Server to it, and attach our app to the
 * server as the handler. Then we run the event loop.
 */
$loop = $container->get(LoopInterface::class);
$tcpSocket = new TcpSocket(8000, $loop);
$httpServer = new HttpServer([$app, 'handle']);
$httpServer->listen($tcpSocket);
print 'Web server running at http://127.0.0.1:8000'.PHP_EOL;
$loop->run();