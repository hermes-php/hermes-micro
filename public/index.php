<?php

require_once __DIR__.'/../vendor/autoload.php';

/**
 * We build the container
 * @var $container \Psr\Container\ContainerInterface
 */
$container = include __DIR__.'/../config/container.php';
/**
 * We fetch the Application out of it. This one is an instance of Runnable, with
 * all the dependencies ready to handle the request.
 * @var $app \Hermes\HttpApp\Runnable
 */
$app = $container->get(\Hermes\HttpApp\Runnable::class);
include __DIR__.'/../config/app-config.php';

$app->run();