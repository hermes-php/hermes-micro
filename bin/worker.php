<?php

require_once __DIR__.'/../vendor/autoload.php';

/**
 * We build the container
 * @var $container \Psr\Container\ContainerInterface
 */
$container = include __DIR__.'/../config/container.php';
/**
 * We fetch the Application out of it.
 * @var $app \Hermes\HttpApp\AppInterface
 */
$app = $container->get(\Hermes\HttpApp\AppInterface::class);
include __DIR__.'/../config/app-config.php';
/**
 * We configure Road Runner's relay and psr-7 client.
 */
$streamRelay = new Spiral\Goridge\StreamRelay(STDIN, STDOUT);
$psr7Client = new Spiral\RoadRunner\PSR7Client(new Spiral\RoadRunner\Worker($streamRelay));
/**
 * We start the worker
 */
while ($request = $psr7Client->acceptRequest()) {
    try {
        $psr7Client->respond($app->handle($request));
    } catch (\Throwable $e) {
        $psr7Client->getWorker()->error((string)$e);
    }
}