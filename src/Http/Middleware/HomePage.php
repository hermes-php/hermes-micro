<?php

namespace App\Http\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response\HtmlResponse;

/**
 * This is the HomePage Middleware.
 *
 * Note that implements the MiddlewareInterface. This is an interface defined
 * as a standard by the PHP-FIG. You can read the standard following the link below.
 *
 * A Middleware is basically something that can handle a Request and can return a response.
 * If this Middleware is unable to handle the request to produce a response, then
 * is passed to the next one in the chain.
 *
 * @link https://www.php-fig.org/psr/psr-15/ The PSR-15 Standard
 *
 * @package App\Http
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
class HomePage implements RequestHandlerInterface
{
    /**
     * Handles the Request returning the homepage.
     *
     * Note that this handler is directly using a PSR-7 implementation. This IS
     * NOT recommended as it ties your code to your PSR-7 implementation.
     *
     * Read the docs on how to efficiently create responses from your controllers.
     *
     * @param ServerRequestInterface $request
     *
     * @return ResponseInterface
     */
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        return new HtmlResponse(file_get_contents(__DIR__ . '/../../../views/welcome.html'), 200);
    }
}