<?php

use Hermes\HttpApp\App;
use Hermes\HttpApp\AppInterface;
use Hermes\HttpApp\Runnable;
use Hermes\HttpApp\RunnableApp;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\Http\Message\UriFactoryInterface;
use React\EventLoop\Factory;
use React\EventLoop\LoopInterface;
use Spiral\RoadRunner\Diactoros\StreamFactory;
use Zend\Diactoros\ResponseFactory;
use Zend\Diactoros\ServerRequestFactory;
use Zend\Diactoros\UriFactory;
use Zend\Expressive\Router\FastRouteRouter;
use Zend\Expressive\Router\Middleware\DispatchMiddleware;
use Zend\Expressive\Router\Middleware\MethodNotAllowedMiddleware;
use Zend\Expressive\Router\Middleware\RouteMiddleware;
use Zend\Expressive\Router\RouteCollector;
use Zend\Expressive\Router\RouterInterface;
use Zend\Stratigility\MiddlewarePipe;
use Zend\Stratigility\MiddlewarePipeInterface;

/**
 * SERVICE CONTAINER CONFIGURATION
 *
 * Hermes uses League Container library for its config.
 * You should read their docs.
 *
 * @see https://container.thephpleague.com/3.x/
 */
$container = new \League\Container\Container();
// This means that every time we fetch a service, we get the same instance by default.
$container->defaultToShared();

/**
 * HERMES APPLICATION
 *
 * These services are related to Hermes. They consist basically in the application
 * instances, a route collector and a middleware pipeline.
 */
$container->add(ContainerInterface::class, $container);
$container->add(AppInterface::class, App::class)
    ->addArgument(RouteCollector::class)
    ->addArgument(MiddlewarePipeInterface::class)
    ->addMethodCall('setContainer', [ContainerInterface::class]);
$container->add(Runnable::class, RunnableApp::class)
    ->addArgument(AppInterface::class)
    ->addArgument([ServerRequestFactory::class, 'fromGlobals'])
    ->addArgument('Http\Response\send');
$container->add(MiddlewarePipeInterface::class, MiddlewarePipe::class);
$container->add(RouteCollector::class)
    ->addArgument(RouterInterface::class);
$container->add(RouterInterface::class, FastRouteRouter::class);
/**
 * REACT HTTP SERVICES
 *
 * This service contains the event loop to run our application as a long-running
 * process over a tcp socket.
 */
$container->add(LoopInterface::class, [Factory::class, 'create']);
/**
 * HTTP FACTORIES
 *
 * These are response factories. These services are really useful to avoid coupling
 * your application logic to an specific implementation of PSR-7 Messages. Use them
 * whenever you need to generate a response to the client.
 */
$container->add(ResponseFactoryInterface::class, ResponseFactory::class);
$container->add(StreamFactoryInterface::class, StreamFactory::class);
$container->add(UriFactoryInterface::class, UriFactory::class);
/**
 * MIDDLEWARE SERVICES
 *
 * Here you can register the middleware as services. The order of execution is
 * defined in app-config.php.
 */
$container->add(RouteMiddleware::class)
    ->addArgument(RouterInterface::class);
$container->add(DispatchMiddleware::class);
$container->add(MethodNotAllowedMiddleware::class)
    ->addArgument(function () use ($container) {
        $response = $container->get(ResponseFactoryInterface::class)->createResponse();
        $response->getBody()->write('405 - Method Not Allowed');
        return $response;
    });


return $container;