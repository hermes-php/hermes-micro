<?php

/**
 * APP CONFIGURATION
 *
 * Here is where you configure your application, like middleware piping and
 * routing.
 *
 * Keep in mind that the middleware pipe works as a FIFO Stack: the middleware
 * that you pipe first is the first middleware to be executed in the chain.
 *
 * You can pass service names in every argument. If the application has a container
 * instance attached, it will be able to resolve them.
 *
 * @var $app \Hermes\HttpApp\AppInterface
 * @var $container \Psr\Container\ContainerInterface
 */

use App\Http\Middleware\HomePage;
use Psr\Http\Message\ResponseFactoryInterface;
use Zend\Expressive\Router\Middleware\DispatchMiddleware;
use Zend\Expressive\Router\Middleware\MethodNotAllowedMiddleware;
use Zend\Expressive\Router\Middleware\RouteMiddleware;

$app->pipe(RouteMiddleware::class);
$app->pipe(DispatchMiddleware::class);
$app->pipe(MethodNotAllowedMiddleware::class);
$app->pipe(function () use ($container) {
    $response = $container->get(ResponseFactoryInterface::class)->createResponse();
    $response->getBody()->write('404 - Not Found');
    return $response;
});

$app->get('/', new HomePage(), 'homepage');

